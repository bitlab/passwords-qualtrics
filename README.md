# Passwords - Qualtrics

This project looks through a database of passwords (e.g. the RockYou dataset)
and generates a new Qualtrics survey file that can be imported into Qualtrics.
The survey will test the correlations between passwords and some user-selected
criteria like entropy.

### Use

```
$ python passwords.py
```

This command will read in the rockyou.txt dataset (14M+ passwords) and caluclate a number of statistics on that dataset

```
$ python password_survey.py
```

This command will grab the first 60 passwords in that data file and generate a survey with 3 blocks, asking about pairs of passwords.

The plan in the future it to combine these two to strategically choose a set of passwords to compare.  We want to create 3 types of comparisons:
* passwords that are very close in entropy (< 3 bits different)
* passwords that are very far apart in entropy (> 20 bits different)
* passwords that are in between (3 < x < 20 bits different)
* an attention check -- a comparison question where both passwords are identical

For each of those classes, we want to pick:
* a password that is fairly weak (< 25 percentile)
* a password that is fairly strong (> 75 percentile)
* a password that is in the middle

Within each class, we should pick passwords randomly

However, it isn't clear how to do this -- what does strong pair that are very far apart look like?

# Importing into qualtrics

Once it work, the survey can be imported into Qualtrics.  

First, save the output as a text file:

```
$ python password_survey.py > pwd.txt
```

Next, go to Qualtrics, and open up a new survey.  Under "Tools", choose "Import
Survey".   Choose this pwd.txt file and click Import.   This will import all of
the questions, and add them to the current survey. So the default block with
the first default question will still be there.   Also note that there is no
way of specifying survey flows in the txt file, so those need to be done
manually.  It also does not add the consent form.


# TODOs

X Write code to save statistics to a file from passwords.py
X Write code to read statistics in from file (json.load)
X Write code to read in (pwd, entropy) tuples
X Write code to create subsamples of the full list of passwords, using the statistics as guides
* Modify survey code to create 5 randomly generated sets of password pairs
* Write code to create 5 sets of blocks, staggered, from those sets of password pairs
