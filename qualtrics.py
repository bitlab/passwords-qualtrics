# This file provides some infrastructure for creating text files that Qualtrics will import correctly
#
# Produces a .txt file formatted as a Qualtrics AdvancedFormat survey file
# https://www.qualtrics.com/support/survey-platform/survey-module/survey-tools/import-and-export-surveys/

class Qualtrics:
	def __init__(self):
		self.out = [self.tag("AdvancedFormat")]

	def tag(self, name, *args):
		args = [str(x) for x in args if x is not None]
		return "[[{}]]".format(":".join([name]+args))

	def embedded_data(self, name, value = None):
		self.out.append(self.tag("ED", name, value))

	def block(self, name=None):
		self.out.append("")
		self.out.append(self.tag("Block", name))

	def page(self):
		self.out.append(self.tag("PageBreak"))

	def id(self, id):
		self.out.append(self.tag("ID", id))

	def question(self, type, subtype = None, subsubtype=None, id=None):
		self.out.append(self.tag("Question", type, subtype, subsubtype))
		if id:
			self.id(id) 

	def choices(self, *choices):
		self.out.append(self.tag("Choices"))
		self.out += [str(x) for x in choices]

	def answers(self, *answers):
		self.out.append(self.tag("Answers"))
		self.out += [str(x) for x in answers]

	def multiple_choice(self, text, *choices, type="SingleAnswer", orientation=None, id=None):
		self.question("MC", type, orientation, id)
		self.out.append(text)
		if len(choices) > 0:
			self.choices(*choices)

	def matrix(self, text, choices=None, answers=None, id=None):
		self.question("Matrix", id=id)
		if choices:
			self.choices(*choices)
		if answers:
			self.answers(*answers)
	
	def text(self, text, id=None):
		self.question("DB", id)
		self.out.append(text)


		
	def __str__(self):
		return "\n".join(self.out)



def test_survey():
	q = Qualtrics()
	q.embedded_data("data_without_value")
	q.embedded_data("data_with_value", 12)

	q.block()
	q.multiple_choice("First question")
	q.choices("One", "Two", "Three")

	q.block("Named_Block")

	q.multiple_choice("Second Question", "One", "Two", "Five")
	q.page()
	q.multiple_choice("Horizontal Q", "H1", "H2", "H3", orientation="Horizontal")
	q.multiple_choice("Vertical Q", "V1", "V2", "V3", orientation="Vertical")
	q.multiple_choice("ID Question", "A1", "A2", "A3", "A4", "A5", id="MyID")

	q.block()
	q.matrix("Matrix Q")
	q.choices("Statement 1", "Statement 2", "Statement 3")
	q.answers("Option left", "Option center", "Option Right")
	
	q.text("This is a plain text block")
	return q

if __name__ == "__main__":
	q = test_survey()
	print(q)
