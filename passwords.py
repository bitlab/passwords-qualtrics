# This file contains code that reads in a file of passwords (one per line),
# calculates the entropy of each password, and then groups them into bins for
# random selection

import re
import math
import statistics
import numpy as np
import json
import sys
import argparse
import functools
import random

perror = functools.partial(print, file=sys.stderr)

# Functions to calculate the entropy of a given password
# This exactly mimics the entropy calculations used in the BITLabStudyAssistant used in the actual study

def parse_args():
	parser = argparse.ArgumentParser(description="Calculate entropy distribution statistics from a password file")
	parser.add_argument("-f", "--file", type=argparse.FileType("r", encoding="latin-1"), default="rockyou.txt", help="File containing one password per row")
	parser.add_argument("-o", "--output", type=argparse.FileType("w"), default="-", help="File to write output to (Default: stdout)")
	parser.add_argument("-e", "--entropy", action='store_true', help="Output per-password entropy measures instead of summary statistics")
	parser.add_argument("-n", "--num", type=int, action='store', default=20, help="Number of passwords to calculate entropy for (only used with -e) or print per decile (for -r)")
	parser.add_argument("-s", "--stats", type=argparse.FileType("r"), help="Load stats from file")
	parser.add_argument("-r", "--random", action='store_true', help="Choose <random> passwords from each decile.  Must specify stats file")
	return parser.parse_args()
	

# Measure the number of characters in the character sets used in a password
def character_set_size(password):
	size = 0
	if re.search(r'[a-z]', password):
		size += 26
	if re.search(r'[A-Z]', password):
		size += 26
	if re.search(r'[01-9]', password):
		size += 10
	if re.search(r'[\!\@\#\$\%\^\&\*\(\)\-\_\+\=\~\`\[\]\{\}\|\\\:\;\"\'\<\>\,\.\?\/]', password):
		if re.search(r'[\^\~\`\[\]\{\}\|\:\;\<\>]', password):
			size += 32
		else:
			size += 20
	if size == 0:  # Not part of original calculation; used for passwords that are solely non-standard characters
		size = 128
	return size

# Measure the length of a password
def pwd_length(password):
	return len(password)

# Calculate the entropy of a password
def entropy(password):
	try:
		return math.log2(character_set_size(password) ** pwd_length(password))
	except:
		print("Entropy error: '{}'".format(password))
		return math.log2(character_set_size(password) ** pwd_length(password))

# For debugging
def pwd_info(password, deciles):
	print("{}: {} chars, {} long, entropy={}; decile={}".format(password, character_set_size(password), pwd_length(password), entropy(password), password_decile(entropy(password), deciles)))


# Read in a file of passwords.  Assumes one password per line
# Creates a generator that you can use in a for loop:
#     for pwd in password_file("file")
# if print_every is set to None, then it doesn't print updates about reading the file
def password_file(f, encoding="latin-1", print_every = 1000000):
	if print_every is not None:
		perror("Reading {}".format(f.name), end="", flush=True)
	i = 0
	for pwd in f:
		pwd = pwd.strip()
		i += 1
		if (print_every is not None) and (i % print_every == 0):
			perror(".", end="", flush=True)
		yield pwd
	if print_every is not None:
		perror("", flush=True) # newline to end reading line

def file_info(fname, num, stats):
	i = 0
	for pwd in password_file(fname, print_every = None):
		pwd_info(pwd, stats['deciles'])
		i += 1
		if i > num:
			return
		
		
def entropy_distribution(fname):
	e = []
	for pwd in password_file(fname):
		e.append(entropy(pwd))
	info = {}
	info['file'] = fname.name
	info['median'] = statistics.median(e)
	info['mean'] = statistics.mean(e)
	info['sd'] = statistics.stdev(e)
	info['min'] = min(e)
	info['max'] = max(e)
	info['num'] = len(e)
	for p in [10, 20, 25, 30, 33, 40, 50, 60, 66, 70, 75, 80, 90]:
		info["q{}".format(p)] = np.quantile(e, p / 100.0)
	return info
	
def save_entropy(entropy, file):
	json.dump(entropy, fp=file, sort_keys=True, indent=4)

def calculate_cutpoints(entropy):	
	entropy['thirds'] = list(map(lambda p: entropy["q{}".format(p)], [33,66]))
	entropy['quartiles'] = list(map(lambda p: entropy["q{}".format(p)], [25,50,75]))
	entropy['deciles'] = list(map(lambda p: entropy["q{}".format(p)], range(10,100,10)))
	return entropy

def load_entropy(file):
	entropy = json.load(file)
	entropy = calculate_cutpoints(entropy)
	return entropy

def password_decile(pwd_entropy, deciles):
	return 10-len(list(filter(lambda c: c, map(lambda x: pwd_entropy <= x, deciles))))

def passwords_by_decile(file, deciles):
	passwords = list(map(lambda x: [], range(1,11,1)))
	for pwd in password_file(file):
		pwd_entropy = entropy(pwd)
		decile = password_decile(pwd_entropy, deciles)
		passwords[decile-1].append(pwd)
	return passwords

def print_random_pwds(decile, pwd_list, num):
	pwds = random.sample(pwd_list, num)
	print("*** {}".format(decile))
	for pwd in pwds:
		print(pwd)
	

if __name__ == "__main__":
	info = parse_args()
	if info.stats:
		perror("Loading stats from {}".format(info.stats.name))
		stats = load_entropy(info.stats)
	else:
		stats = entropy_distribution(info.file)
		
	if info.random:
		deciles = passwords_by_decile(info.file, stats['deciles'])
		for i in range(0,10):
			print_random_pwds(i+1, deciles[i], info.num)
	elif info.entropy:
		out = file_info(info.file, info.num, stats)
	else:
		save_entropy(stats, info.output)
		
