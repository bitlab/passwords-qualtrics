from qualtrics import Qualtrics
from passwords import password_file, load_entropy, passwords_by_decile
import argparse
import random

def parse_args():
	parser = argparse.ArgumentParser()
	parser.add_argument("-s", "--stats", type=argparse.FileType("r"), help="Load stats from file", required=True)
	parser.add_argument("-o", "--output", type=argparse.FileType("w"), default="-", help="File to write output to (Default: stdout)")
	parser.add_argument("-f", "--file", type=argparse.FileType("r", encoding="latin-1"), default="rockyou.txt", help="File containing one password per row")
	return parser.parse_args()
	

# Passwords for debugging
def random_passwords(fname, num = 100):
	i = 0
	pwds = []
	for pwd in password_file(fname, print_every=None):
		pwds.append(pwd)
		i += 1
		if i > num:
			break
	return pwds

def debug_pairs(pwds, index, length):
	start = (index-1)*(length*2)
	return zip(pwds[start:start+length], pwds[start+length+1:start+(length*2)])


# Choose a set of passwords intelligently

def looks_like_email(pwd):
	if "@" in pwd:
		return True
	return False

def contains_bad_word(pwd):
	bad_words = ['shit', 'fuck', 'pussy', 'cunt', 'ass', 'cock', 'tits'] # Seven dirty words
	for word in bad_words:
		if word in pwd:
			return True
	return False
		
def random_password(pwd_set):
	pwd = random.choice(pwd_set)
	if (looks_like_email(pwd) or contains_bad_word(pwd)):
		return random_password(pwd_set)  
	return pwd


def random_password_pair(pwd_deciles, dec1, dec2):
	# Randomize order
	if random.choice((True, False)):
		return (random_password(pwd_deciles[dec1]), random_password(pwd_deciles[dec2]))
	else:
		return (random_password(pwd_deciles[dec2]), random_password(pwd_deciles[dec1]))

def attention_check_pair(pwd_deciles, decile):
	pwd = random_password(pwd_deciles[decile])
	return (pwd, pwd)

def password_set(pwd_deciles):
	# Remember, deciles are numbered 0-9 (not 1-10)
	return [
		random_password_pair(pwd_deciles, 1, 4),
		random_password_pair(pwd_deciles, 1, 5),
		random_password_pair(pwd_deciles, 1, 6),
		random_password_pair(pwd_deciles, 1, 7),
		random_password_pair(pwd_deciles, 1, 8),
		random_password_pair(pwd_deciles, 2, 5),
		random_password_pair(pwd_deciles, 2, 6),
		random_password_pair(pwd_deciles, 2, 7),
		random_password_pair(pwd_deciles, 2, 8),
		random_password_pair(pwd_deciles, 2, 9),
		random_password_pair(pwd_deciles, 3, 6),
		random_password_pair(pwd_deciles, 3, 7),
		random_password_pair(pwd_deciles, 3, 8),
		random_password_pair(pwd_deciles, 3, 9),
		random_password_pair(pwd_deciles, 4, 7),
		random_password_pair(pwd_deciles, 4, 8),
		random_password_pair(pwd_deciles, 4, 9),
		random_password_pair(pwd_deciles, 5, 8),
		random_password_pair(pwd_deciles, 5, 9),
		random_password_pair(pwd_deciles, 6, 9),
		attention_check_pair(pwd_deciles, random.randint(1,8))
	]


# Generate parts of questions
def password_comparison_text(comparision, pwd1, pwd2):
	return """
Which password is {}?<br />
<div style="width: 50%; float: left; text-align: center; font-size: large; font-weight: bold">{}</div> <div style="width: 50%; float: left; text-align: center; font-size: large; font-weight: bold">{}</div>
		""".format(comparision, pwd1, pwd2).strip()

def password_options(comparison, pwd1, pwd2):
	return [
		"'<b>{}</b>' is {}".format(pwd1, comparison),
		"both are equal",
		"'<b>{}</b>' is {}".format(pwd2, comparison)
		]

# Generate lists of questions
def easier_to_remember(q, id, pwd_pairs):
	q.block("Remember{}".format(id))
	q.text("""
The questions below ask you to compare two different potential passwords.  For
each question, consider the two passwords and ask yourself which password would
be <b>easier for you to remember</b> if you used it as your password?
	""".strip())
	for pair in pwd_pairs:
		q.multiple_choice(password_comparison_text("easier to remember", pair[0], pair[1]), orientation="Horizontal")
		q.choices(*password_options("easier to remember", pair[0], pair[1]))


def easier_to_type(q, id, pwd_pairs):
	q.block("Type{}".format(id))
	q.text("""
The questions below ask you to compare two different potential passwords.  For
each question, consider the two passwords and ask yourself which password would
be <b>easier for you to type into a computer, tablet, or mobile device</b> if you used it as your password?
	""".strip())
	for pair in pwd_pairs:
		q.multiple_choice(password_comparison_text("easier to type", pair[0], pair[1]), orientation="Horizontal")
		q.choices(*password_options("easier to type", pair[0], pair[1]))

def more_secure(q, id, pwd_pairs):
	q.block("Secure{}".format(id))
	q.text("""
The questions below ask you to compare two different potential passwords.  For
each question, consider the two passwords and ask yourself which password would
be <b>more secure and harder for others to guess</b> if you used it as your password?
	""".strip())
	for pair in pwd_pairs:
		q.multiple_choice(password_comparison_text("more secure", pair[0], pair[1]), orientation="Horizontal")
		q.choices(*password_options("more secure", pair[0], pair[1]))




if __name__ == "__main__":
	info = parse_args()
	q = Qualtrics()
	stats = load_entropy(info.stats)
	pwds = passwords_by_decile(info.file, stats['deciles'])

	psets = list(map(lambda x: password_set(pwds), range(5)))

	for i in range(5):
		easier_to_remember(q, i % 5 + 1, psets[i % 5])
		easier_to_type(q, (i+1) % 5 + 1, psets[(i+1) % 5])
		more_secure(q, (i+2) % 5 + 1, psets[(i+2) % 5])

	print(q, file=info.output)
	
